#include "vtkActor.h"
#include "vtkAnimationCue.h"
#include "vtkAnimationScene.h"
#include "vtkCamera.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkDataArray.h"
#include "vtkGLTFImporter.h"
#include "vtkGLTFMapper.h"
#include "vtkGLTFReader.h"
#include "vtkImageData.h"
#include "vtkImageFlip.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkJPEGReader.h"
#include "vtkLightKit.h"
#include "vtkOpenGLRenderer.h"
#include "vtkOpenGLSkybox.h"
#include "vtkOpenGLTexture.h"
#include "vtkPBRIrradianceTexture.h"
#include "vtkPBRLUTTexture.h"
#include "vtkPBRPrefilterTexture.h"
#include "vtkPointData.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkTexture.h"
#include "vtk_jsoncpp.h"

// TODO: custom animation selection

class vtkTimerCallback2 : public vtkCommand
{
public:
  static vtkTimerCallback2* New()
  {
    vtkTimerCallback2* cb = new vtkTimerCallback2;
    cb->TimerCount = 0;
    return cb;
  }

  virtual void Execute(vtkObject* caller, unsigned long eventId, void* vtkNotUsed(callData))
  {
    if (vtkCommand::TimerEvent == eventId)
    {
      ++this->TimerCount;
    }
    vtkInformation* readerInfo = this->Reader->GetOutputInformation(0);
    int nbSteps = readerInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
    if (this->TimerCount >= nbSteps)
    {
      this->TimerCount = 0;
    }
    double step = readerInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), this->TimerCount);
    readerInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), step);
    vtkRenderWindowInteractor* iren = dynamic_cast<vtkRenderWindowInteractor*>(caller);
    iren->GetRenderWindow()->Render();
  }

private:
  int TimerCount;

public:
  vtkActor* actor;
  vtkGLTFReader* Reader;
};

void testReader(std::string filename, bool applyFirstTexture, int texId)
{
  vtkNew<vtkGLTFReader> reader;

  reader->SetFileName(filename.c_str());
  reader->SetApplyDeformationsToGeometry(false);
  reader->SetFrameRate(60);
  // Fetch reader information
  reader->UpdateInformation();
  vtkInformation* info = reader->GetOutputInformation(0);

  int nAnim = reader->GetNumberOfAnimations();
  int nScenes = reader->GetNumberOfScenes();

  // Get animation names and durations
  std::string animNames[nAnim];
  for (int i = 0; i < nAnim; i++)
  {
    std::cout << reader->GetAnimationName(i) << std::endl;
    animNames[i] = reader->GetAnimationName(i);
  }
  // Get scene names
  std::string sceneNames[nScenes];
  for (int i = 0; i < nScenes; i++)
  {
    sceneNames[i] = reader->GetSceneName(i);
  }
  // Select all animations for testing
  // for (int i = 0; i < nAnim; i++)
  // {
  //   reader->EnableAnimation(i);
  // }

  reader->EnableAnimation(0);
  // Select scene
  reader->SetCurrentScene(0);

  vtkNew<vtkGLTFMapper> mapper;
  vtkNew<vtkActor> actor;
  vtkNew<vtkOpenGLRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkInteractorStyleTrackballCamera> style;

  double color[3] = { 0.3, 0.3, 0.5 };
  renderer->SetBackground(color);

  mapper->SetInputConnection(reader->GetOutputPort());
  actor->SetMapper(mapper);
  renderer->AddActor(actor);

  auto camera = renderer->GetActiveCamera();
  double range[2] = { 0.00001, 100000 };
  camera->SetClippingRange(range);
  // flip texture coordinates
  if (actor->GetPropertyKeys() == nullptr)
  {
    vtkNew<vtkInformation> info;
    actor->SetPropertyKeys(info);
  }
  double mat[] = { 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
  actor->GetPropertyKeys()->Set(vtkProp::GeneralTextureTransform(), mat, 16);
  // actor->ForceTranslucentOn();

  reader->Update();
  std::vector<vtkSmartPointer<vtkTexture> > textures;
  for (int i = 0; i < reader->GetNumberOfTextures(); i++)
  {
    // create vtk textures
    auto readerTex = reader->GetGLTFTexture(i);
    vtkNew<vtkTexture> tex;
    tex->SetInputData(readerTex.Image);
    tex->InterpolateOn();
    tex->MipmapOn();
    tex->Update();
    // tex->SetBlendingMode(vtkTexture::VTK_TEXTURE_BLENDING_MODE_INTERPOLATE);
    textures.push_back(tex);
  }

  mapper->SetTextures(textures);
  mapper->SetColorModeToDirectScalars();
  mapper->SetInterpolateScalarsBeforeMapping(true);
  renderer->SetUseFXAA(true);

  renderer->UseDepthPeelingOn();
  renderWindow->AddRenderer(renderer);
  renderWindow->SetPosition(1920, 0);
  renderWindow->SetSize(1280, 720);
  renderWindow->SetAlphaBitPlanes(1);
  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(style);
  renderWindowInteractor->Initialize();

  if (nAnim > 0)
  {
    reader->SetApplyDeformationsToGeometry(false);
    // Get range
    double maxRange = 0;
    maxRange = info->Get(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), 1);

    renderWindowInteractor->Initialize();
    vtkSmartPointer<vtkTimerCallback2> cb = vtkSmartPointer<vtkTimerCallback2>::New();
    cb->Reader = reader;
    renderWindowInteractor->AddObserver(vtkCommand::TimerEvent, cb);
    int timerId = renderWindowInteractor->CreateRepeatingTimer(16);
  }

  renderer->GetActiveCamera()->Azimuth(30);
  renderer->GetActiveCamera()->Elevation(30);
  renderer->GetActiveCamera()->SetClippingRange(0.0001, 10000);
  renderWindowInteractor->Start();
}

int main(int argc, char** argv)
{
  std::string dataRoot = "data/";
  std::string assetName = "CesiumMan";
  std::string type = "";

  if (argc >= 2)
  {
    assetName = std::string(argv[1]);
  }
  if (argc >= 3)
  {
    type = std::string(argv[2]);
  }

  std::string subdir = "/glTF";
  std::string ext = ".gltf";

  bool applyTex = false;
  int texId = 0;
  if (type == "bin")
  {
    subdir += "-Binary";
    ext = ".glb";
  }
  else if (type == "tex")
  {
    applyTex = true;
    if (argc >= 4)
    {
      texId = argv[3][0] - '0';
    }
  }
  std::string filename = dataRoot + "/" + assetName + subdir + "/" + assetName + ext;

  testReader(filename, false, 0);
  return 1;
}
