#include "vtkActor.h"
#include "vtkAnimationCue.h"
#include "vtkAnimationScene.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkGLTFImporter.h"
#include "vtkGLTFMapper.h"
#include "vtkGLTFMapper2.h"
#include "vtkGLTFReader.h"
#include "vtkImageData.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkTexture.h"
#include "vtk_jsoncpp.h"

#define USEMAPPER2
#ifdef USEMAPPER2
#define MAPPER vtkGLTFMapper2
#else
#define MAPPER vtkGLTFMapper
#endif

class vtkAnimationCueObserver : public vtkCommand
{
public:
  static vtkAnimationCueObserver* New() { return new vtkAnimationCueObserver; }

  // Tick callback for animationCue
  virtual void Execute(
    vtkObject* vtkNotUsed(caller), unsigned long event, void* vtkNotUsed(calldata))
  {
    if (this->RenWin != 0)
    {
      if (event == vtkCommand::AnimationCueTickEvent)
      {
        // Tick
        vtkInformation* readerInfo = this->Reader->GetOutputInformation(0);
        int nbSteps = readerInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
        if (this->StepId >= nbSteps)
        {
          this->StepId = 0;
        }
        double step = readerInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), this->StepId);
        readerInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), step);
        this->Reader->Update();
        // this->Mapper->SetupMappersForGLTF();
        this->RenWin->Render();
        this->StepId++;
      }
    }
  }
  int StepId = 0;
  vtkRenderWindow* RenWin;
  vtkRenderer* renderer;
  vtkGLTFReader* Reader;
};

void testReader(std::string filename)
{
  vtkNew<vtkGLTFReader> reader;

  reader->SetFileName(filename.c_str());
  reader->SetApplyDeformationsToGeometry(false);
  // Fetch reader information
  reader->UpdateInformation();
  vtkInformation* info = reader->GetOutputInformation(0);

  int nAnim = reader->GetNumberOfAnimations();
  int nScenes = reader->GetNumberOfScenes();

  // Get animation names and durations
  std::string animNames[nAnim];
  for (int i = 0; i < nAnim; i++)
  {
    animNames[i] = reader->GetAnimationName(i);
  }
  // Get scene names
  std::string sceneNames[nScenes];
  for (int i = 0; i < nScenes; i++)
  {
    sceneNames[i] = reader->GetSceneName(i);
  }
  // Select all animations for testing
  for (int i = 0; i < nAnim; i++)
  {
    reader->EnableAnimation(i);
  }
  // Select scene
  reader->SetCurrentScene(0);

  vtkNew<vtkGLTFMapper2> mapper;
  vtkNew<vtkActor> actor;
  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkInteractorStyleTrackballCamera> style;

  mapper->SetInputConnection(reader->GetOutputPort());
  actor->SetMapper(mapper);
  // reader->Update();
  renderer->AddActor(actor);

  std::vector<vtkSmartPointer<vtkTexture> > textures;
  textures.resize(reader->GetNumberOfTextures());
  for (int i = 0; i < reader->GetNumberOfTextures(); i++)
  {
    auto vtkTex = textures[i];
    auto gltfTex = reader->GetGLTFTexture(i);
    vtkTex = vtkSmartPointer<vtkTexture>::New();
    vtkTex->SetInputData(gltfTex.Image);
  }
  mapper->SetTextures(textures);

  if (reader->GetNumberOfTextures())
  {

    // create vtk textures
    auto readerTex = reader->GetGLTFTexture(0);
    vtkNew<vtkTexture> tex;
    std::cout << readerTex.Image << std::endl;
    tex->SetInputData(readerTex.Image);
    // actor->SetTexture(tex);
  }

  renderWindow->AddRenderer(renderer);
  renderWindow->SetPosition(1920, 0);
  renderWindow->SetSize(800, 800);
  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(style);
  renderWindowInteractor->Initialize();

  if (nAnim > 0)
  {
    // Get range
    double maxRange = 0;
    maxRange = info->Get(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), 1);
    info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), 15);
    // Setup animation scene
    vtkNew<vtkAnimationScene> scene;
    scene->SetModeToRealTime();
    scene->SetLoop(1);
    scene->SetFrameRate(20);
    reader->SetFrameRate(150);
    scene->SetStartTime(0);
    scene->SetFrameRate(60);
    scene->SetEndTime(maxRange);

    // Setup animation cue
    vtkNew<vtkAnimationCue> cue;
    cue->SetStartTime(0);
    cue->SetEndTime(maxRange);
    scene->AddCue(cue);
    // Setup observer
    vtkNew<vtkAnimationCueObserver> observer;
    observer->Reader = reader;
    observer->RenWin = renderWindow;
    observer->renderer = renderer;
    // observer->Mapper = mapper;
    cue->AddObserver(vtkCommand::AnimationCueTickEvent, observer);
    reader->Update();
    mapper->Update();
    renderWindow->Render();
    // Play animation
    renderWindowInteractor->SetRenderWindow(renderWindow);
    renderWindowInteractor->Start();
    scene->Play();
  }

  reader->Update();
#ifndef USEMAPPER2
  mapper->SetupMappersForGLTF();
  std::cout << "using second mapper" << std::endl;
#endif
  renderWindow->Render();
  renderWindowInteractor->Start();
}

int main(int argc, char** argv)
{
  std::string dataRoot = "data/";
  std::string assetName = "CesiumMan";
  std::string type = "";

  if (argc >= 2)
  {
    assetName = std::string(argv[1]);
  }
  if (argc >= 3)
  {
    type = std::string(argv[2]);
  }

  std::string subdir = "/glTF";
  std::string ext = ".gltf";
  if (type == "bin")
  {
    subdir += "-Binary";
    ext = ".glb";
  }
  std::string filename = dataRoot + "/" + assetName + subdir + "/" + assetName + ext;
  testReader(filename);
  return 1;
}
