#include "vtkActor.h"
#include "vtkAnimationCue.h"
#include "vtkAnimationScene.h"
#include "vtkCamera.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkGLTFImporter.h"
#include "vtkGLTFReader.h"
#include "vtkImageFlip.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkJPEGReader.h"
#include "vtkMath.h"
#include "vtkOpenGLRenderer.h"
#include "vtkOpenGLSkybox.h"
#include "vtkOpenGLTexture.h"
#include "vtkPBRIrradianceTexture.h"
#include "vtkPBRLUTTexture.h"
#include "vtkPBRPrefilterTexture.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtk_jsoncpp.h"

void testImporter(std::string filename)
{
  vtkNew<vtkOpenGLRenderer> renderer;
  renderer->SetBackground(0.2, 0.2, 0.2);
  vtkNew<vtkRenderWindow> renderWindow;
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkGLTFImporter> importer;

  vtkNew<vtkInteractorStyleTrackballCamera> style;

  //************************************ Add skybox

  vtkNew<vtkOpenGLSkybox> skybox;

  vtkSmartPointer<vtkPBRIrradianceTexture> irradiance = renderer->GetEnvMapIrradiance();
  // irradiance->SetIrradianceStep(3);
  vtkSmartPointer<vtkPBRPrefilterTexture> prefilter = renderer->GetEnvMapPrefiltered();
  // prefilter->SetPrefilterSamples(1024);
  // prefilter->SetPrefilterSize(1024);

  vtkNew<vtkOpenGLTexture> textureCubemap;
  textureCubemap->CubeMapOn();

  std::string pathSkybox[6] = { "data/skyboxes/mountain/0.jpg", "data/skyboxes/mountain/1.jpg",
    "data/skyboxes/mountain/2.jpg", "data/skyboxes/mountain/3.jpg", "data/skyboxes/mountain/4.jpg",
    "data/skyboxes/mountain/5.jpg" };

  for (int i = 0; i < 6; i++)
  {
    vtkNew<vtkJPEGReader> jpg;
    const char* fname = pathSkybox[i].c_str();
    jpg->SetFileName(fname);
    delete[] fname;
    vtkNew<vtkImageFlip> flip;
    flip->SetInputConnection(jpg->GetOutputPort());
    flip->SetFilteredAxis(1); // flip y axis
    textureCubemap->SetInputConnection(i, flip->GetOutputPort());
  }

  renderer->SetEnvironmentCubeMap(textureCubemap);
  renderer->UseImageBasedLightingOn();
  skybox->SetTexture(textureCubemap);
  renderer->AddActor(skybox);

  //********************************* end skybox

  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(style);
  renderWindowInteractor->Initialize();

  renderWindow->AddRenderer(renderer);
  renderWindow->SetAlphaBitPlanes(1);
  renderWindow->SetPosition(1920, 0);
  renderWindow->SetSize(1280, 720);
  renderWindowInteractor->SetRenderWindow(renderWindow);

  importer->SetFileName(filename.c_str());
  importer->SetRenderWindow(renderWindow);
  importer->Update();

  float step = 0.005;
  renderWindow->Render();
  renderWindowInteractor->Start();
  auto camera = renderer->GetActiveCamera();
  double range[2] = { 0.001, 10000 };
  camera->SetClippingRange(range);
  // double viewUp[3] = { 1.0, -0.5, 1.0 };
  // camera->SetViewUp(viewUp);
  for (float angle = 0; angle < vtkMath::Pi() * 4.0; angle += step)
  {
    camera->Azimuth(vtkMath::DegreesFromRadians(step));
    renderWindow->Render();
  }
  renderWindowInteractor->Start();
}

int main(int argc, char** argv)
{
  std::string dataRoot = "data/";
  std::string assetName = "Duck";
  std::string type = "";

  if (argc >= 2)
  {
    assetName = std::string(argv[1]);
  }
  if (argc >= 3)
  {
    type = std::string(argv[2]);
  }

  std::string subdir = "/glTF";
  std::string ext = ".gltf";
  if (type == "bin")
  {
    subdir += "-Binary";
    ext = ".glb";
  }
  // std::string filename = dataRoot + "/" + assetName + subdir + "/" + assetName + ext;
  std::string filename = assetName;
  std::cout << "opening " << filename << std::endl;
  testImporter(filename);
  return 1;
}
