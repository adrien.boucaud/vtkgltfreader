#include "vtkActor.h"
#include "vtkAnimationCue.h"
#include "vtkAnimationScene.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkGLTFImporter.h"
#include "vtkGLTFReader.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkPNGWriter.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkWindowToImageFilter.h"
#include "vtk_jsoncpp.h"

void testImporter(std::string filename)
{
  vtkNew<vtkRenderer> renderer;
  renderer->SetBackground(0.2, 0.2, 0.2);
  vtkNew<vtkRenderWindow> renderWindow;
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkGLTFImporter> importer;

  vtkNew<vtkInteractorStyleTrackballCamera> style;

  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(style);
  renderWindowInteractor->Initialize();

  renderWindow->AddRenderer(renderer);
  renderWindow->SetAlphaBitPlanes(1);
  renderWindow->SetPosition(1920, 0);
  renderWindow->SetSize(1280, 720);
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderer->SetBackground(1, 1, 1);
  importer->SetFileName(filename.c_str());
  importer->SetRenderWindow(renderWindow);
  importer->Update();

  renderWindow->Render();

  vtkNew<vtkWindowToImageFilter> windowToImageFilter;
  windowToImageFilter->SetInput(renderWindow);
  //   windowToImageFilter->SetMagnification(3); // might be useless
  windowToImageFilter->SetInputBufferTypeToRGBA();
  windowToImageFilter->ReadFrontBufferOff();

  renderWindowInteractor->Start();
  std::cout << "capturing now" << std::endl;
  windowToImageFilter->Update();

  vtkNew<vtkPNGWriter> pngWriter;
  pngWriter->SetFileName("screen.png");
  pngWriter->SetInputConnection(windowToImageFilter->GetOutputPort());
  pngWriter->Write();
}

int main(int argc, char** argv)
{
  std::string dataRoot = "data/";
  std::string assetName = "Duck";
  std::string type = "";

  if (argc >= 2)
  {
    assetName = std::string(argv[1]);
  }
  if (argc >= 3)
  {
    type = std::string(argv[2]);
  }

  std::string subdir = "/glTF";
  std::string ext = ".gltf";
  if (type == "bin")
  {
    subdir += "-Binary";
    ext = ".glb";
  }
  // std::string filename = dataRoot + "/" + assetName + subdir + "/" + assetName + ext;
  std::string filename = assetName;
  std::cout << "opening " << filename << std::endl;
  testImporter(filename);
  return 1;
}
