#include "vtkActor.h"
#include "vtkAnimationCue.h"
#include "vtkAnimationScene.h"
#include "vtkCompositeDataSet.h"
#include "vtkCompositePolyDataMapper.h"
#include "vtkDataObject.h"
#include "vtkDataObjectTreeIterator.h"
#include "vtkFieldData.h"
#include "vtkGLTFImporter.h"
#include "vtkGLTFMapper.h"
#include "vtkGLTFReader.h"
#include "vtkImageData.h"
#include "vtkInformation.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkProperty.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkTexture.h"
#include "vtkWindowToImageFilter.h"
#include "vtk_jsoncpp.h"

class vtkAnimationCueObserver : public vtkCommand
{
public:
  static vtkAnimationCueObserver* New() { return new vtkAnimationCueObserver; }

  // Tick callback for animationCue
  virtual void Execute(
    vtkObject* vtkNotUsed(caller), unsigned long event, void* vtkNotUsed(calldata))
  {
    if (this->RenWin != 0)
    {
      if (event == vtkCommand::AnimationCueTickEvent)
      {
        // Tick
        vtkInformation* readerInfo = this->Reader->GetOutputInformation(0);
        int nbSteps = readerInfo->Length(vtkStreamingDemandDrivenPipeline::TIME_STEPS());
        if (this->StepId >= nbSteps)
        {
          this->StepId = 0;
        }
        double step = readerInfo->Get(vtkStreamingDemandDrivenPipeline::TIME_STEPS(), this->StepId);
        readerInfo->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), step);
        this->Reader->Modified();
        this->RenWin->Render();
        this->StepId++;
      }
    }
  }
  int StepId = 0;
  vtkRenderWindow* RenWin;
  vtkRenderer* renderer;
  vtkGLTFReader* Reader;
};

void testReader(std::string filename, bool applyFirstTexture, int texId)
{
  vtkNew<vtkGLTFReader> reader;

  reader->SetFileName(filename.c_str());
  reader->SetApplyDeformationsToGeometry(true);
  // Fetch reader information
  reader->UpdateInformation();
  vtkInformation* info = reader->GetOutputInformation(0);

  int nAnim = reader->GetNumberOfAnimations();
  int nScenes = reader->GetNumberOfScenes();

  // Get animation names and durations
  std::string animNames[nAnim];
  for (int i = 0; i < nAnim; i++)
  {
    animNames[i] = reader->GetAnimationName(i);
  }
  // Get scene names
  std::string sceneNames[nScenes];
  for (int i = 0; i < nScenes; i++)
  {
    sceneNames[i] = reader->GetSceneName(i);
  }
  // Select all animations for testing
  for (int i = 0; i < nAnim; i++)
  {
    reader->EnableAnimation(i);
  }
  // Select scene
  reader->SetCurrentScene(0);

  vtkNew<vtkCompositePolyDataMapper> mapper;
  vtkNew<vtkActor> actor;
  vtkNew<vtkRenderer> renderer;
  vtkNew<vtkRenderWindow> renderWindow;
  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  vtkNew<vtkInteractorStyleTrackballCamera> style;

  mapper->SetInputConnection(reader->GetOutputPort());
  actor->SetMapper(mapper);
  reader->Update();

  // BusterDrone specific. Demo stuff
  // auto iterator =
  //   vtkMultiBlockDataSet::SafeDownCast(reader->GetOutputDataObject(0))->NewTreeIterator();
  // iterator->SetSkipEmptyNodes(true);
  // iterator->InitTraversal();
  // for (; !iterator->IsDoneWithTraversal(); iterator->GoToNextItem())
  // {
  //   auto poly = vtkPolyData::SafeDownCast(iterator->GetCurrentDataObject());
  //   poly->GetPointData()->PrintSelf(std::cout, vtkIndent());
  //   std::cout << "array: " << poly->GetPointData()->GetArray("texcoord_1") << std::endl;
  //   poly->GetPointData()->SetTCoords(poly->GetPointData()->GetArray("texcoord_1"));
  // }
  //  for (auto it)
  // End

  renderer->AddActor(actor);

  std::vector<vtkSmartPointer<vtkTexture> > textures;
  textures.resize(reader->GetNumberOfTextures());
  for (int i = 0; i < reader->GetNumberOfTextures(); i++)
  {
    auto vtkTex = textures[i];
    auto gltfTex = reader->GetGLTFTexture(i);
    vtkTex = vtkSmartPointer<vtkTexture>::New();
    vtkTex->SetInputData(gltfTex.Image);
  }

  if (reader->GetNumberOfTextures() && applyFirstTexture)
  {
    // create vtk textures
    auto readerTex = reader->GetGLTFTexture(texId);
    vtkNew<vtkTexture> tex;
    std::cout << readerTex.Image << std::endl;
    tex->SetInputData(readerTex.Image);
    actor->SetTexture(tex);
  }

  renderWindow->AddRenderer(renderer);
  renderWindow->SetPosition(1920, 0);
  renderWindow->SetSize(1280, 720);
  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(style);
  renderWindowInteractor->Initialize();

  if (nAnim > 0)
  {
    reader->SetApplyDeformationsToGeometry(true);
    // Get range
    double maxRange = 0;
    maxRange = info->Get(vtkStreamingDemandDrivenPipeline::TIME_RANGE(), 1);
    info->Set(vtkStreamingDemandDrivenPipeline::UPDATE_TIME_STEP(), 15);
    // Setup animation scene
    vtkNew<vtkAnimationScene> scene;
    scene->SetModeToRealTime();
    scene->SetLoop(1);
    scene->SetFrameRate(20);
    reader->SetFrameRate(150);
    scene->SetStartTime(0);
    scene->SetFrameRate(60);
    scene->SetEndTime(maxRange);

    // Setup animation cue
    vtkNew<vtkAnimationCue> cue;
    cue->SetStartTime(0);
    cue->SetEndTime(maxRange);
    scene->AddCue(cue);
    // Setup observer
    vtkNew<vtkAnimationCueObserver> observer;
    observer->Reader = reader;
    observer->RenWin = renderWindow;
    observer->renderer = renderer;
    // observer->Mapper = mapper;
    cue->AddObserver(vtkCommand::AnimationCueTickEvent, observer);
    reader->Update();
    mapper->Update();
    renderWindow->Render();
    // Play animation
    renderWindowInteractor->SetRenderWindow(renderWindow);
    renderWindowInteractor->Start();
    scene->Play();
  }

  reader->Update();
  renderWindow->Render();
  renderWindowInteractor->Start();
}

int main(int argc, char** argv)
{
  std::string dataRoot = "data/";
  std::string assetName = "CesiumMan";
  std::string type = "";

  if (argc >= 2)
  {
    assetName = std::string(argv[1]);
  }
  if (argc >= 3)
  {
    type = std::string(argv[2]);
  }

  std::string subdir = "/glTF";
  std::string ext = ".gltf";

  bool applyTex = false;
  int texId = 0;
  if (type == "bin")
  {
    subdir += "-Binary";
    ext = ".glb";
  }
  else if (type == "tex")
  {
    applyTex = true;
    if (argc >= 4)
    {
      texId = argv[3][0] - '0';
    }
  }
  std::string filename = dataRoot + "/" + assetName + subdir + "/" + assetName + ext;
  testReader(filename, applyTex, texId);
  return 1;
}
